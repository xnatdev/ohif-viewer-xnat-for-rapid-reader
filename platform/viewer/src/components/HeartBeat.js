import React, { useEffect } from 'react';
import { fetchUserInfoForEmbed } from '@ohif/extension-rapid-reader';
import getXnatUrl from '../utils/getXnatUrl';

const PERIOD_IN_MS = 30000;

export default function HeartBeat() {
  useEffect(() => {
    let handler;
    const xnatUrl = getXnatUrl();
    async function doPerodicCheck() {
      handler = undefined;
      try {
        await fetchUserInfoForEmbed(xnatUrl);
        handler = setTimeout(doPerodicCheck, PERIOD_IN_MS);
      } catch (error) {
        location.href = xnatUrl + '/rapid-reader';
      }
    }

    handler = setTimeout(doPerodicCheck, PERIOD_IN_MS);
    return () => {
      if (handler) {
        clearTimeout(handler);
        handler = undefined;
      }
    };
  }, []);
  return <div></div>;
}
