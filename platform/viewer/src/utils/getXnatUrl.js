export default function getXnatUrl() {
  let xnatUrl;
  if (process.env.NODE_ENV === 'development') {
    xnatUrl = `${window.location.origin}${process.env.XNAT_PROXY}`;
    // eslint-disable-next-line no-console
    console.log('PROXY URL:' + xnatUrl);
  } else {
    xnatUrl = `${window.location.origin}`;
  }

  return xnatUrl;
}

// function normalizeUrl(url) {
//   return url.replace(/\/+$/, '');
// }
