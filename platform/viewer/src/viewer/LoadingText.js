import React from 'react';
import { Icon } from '@ohif/ui';
// TODO: Temporarily implemented animation. When Icon supports animation, use it.
import './LoadingText.styl';

function LoadingText() {
  return (
    <div className="loading-text">
      {'Loading'}...{' '}
      <div className="loading-animation">
        <Icon name="circle-notch" />
      </div>
    </div>
  );
}

export default LoadingText;
