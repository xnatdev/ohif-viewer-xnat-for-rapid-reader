import { doGet, doPut, doPost, doFetch } from './util';

export async function fetchPluginCheck(user) {
  const { token, tokenSecret } = user;
  const url = `/xapi/workLists/check`;
  const headers = new Headers();
  const authString = `${token}:${tokenSecret}`;
  headers.set('Authorization', 'Basic ' + btoa(authString));
  await doFetch(url, { headers: headers });
}

export async function fetchUserInfo(user) {
  const { token, tokenSecret } = user;
  const url = `/xapi/workLists/user`;
  const headers = new Headers();
  const authString = `${token}:${tokenSecret}`;
  headers.set('Authorization', 'Basic ' + btoa(authString));
  const response = await doFetch(url, { headers: headers });
  return await response.json();
}

export async function fetchUserInfoForEmbed(xnatUrl) {
  const url = `${xnatUrl}/xapi/workLists/user`;
  const response = await doGet(url);
  return await response.json();
}

export async function fetchWorkLists(xnatUrl, filters) {
  const url = new URL(`${xnatUrl}/xapi/workLists`);
  if (filters) {
    Object.keys(filters).forEach(filter => {
      url.searchParams.append(filter, filters[filter]);
    });

    if (filters.status === '') {
      url.searchParams.append('showActiveOnly', 'true');
    }
  }
  if (!url.searchParams.has('isAsc')) {
    url.searchParams.append('isAsc', 'true');
  }
  const response = await doGet(url.href);
  return await response.json();
}

export async function fetchWorkList(xnatUrl, workListId) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}`;
  const response = await doGet(url);
  return await response.json();
}

export async function fetchWorkItems(xnatUrl, workListId) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/items`;
  const response = await doGet(url);
  return await response.json();
}

export async function fetchMetadata(xnatUrl, projectId, experimentId) {
  const url = `${xnatUrl}/xapi/viewer/projects/${projectId}/experiments/${experimentId}`;
  const response = await doGet(url);
  return await response.json();
}

export async function fetchUpdateWorkListStatus(xnatUrl, workListId, status) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/status/${status}`;
  const response = await doPut(url);
  return await response.json();
}

export async function fetchUploadPdf(xnatUrl, workListId, file) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/sign`;
  const formData = new FormData();
  formData.append('file', file);
  await doFetch(url, {
    method: 'POST',
    body: formData,
  });
}

export async function fetchBeginTimeTrack(xnatUrl, workListId) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/track`;
  await doPost(url);
}

export async function fetchUpdateTimeTrack(xnatUrl, workListId, form) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/track`;
  const headers = new Headers();
  headers.set('Content-Type', 'application/json');
  const options = {
    headers,
    method: 'PUT',
    cache: 'no-cache',
    body: JSON.stringify(form),
  };

  const response = await doFetch(url, options);
  return response;
}
