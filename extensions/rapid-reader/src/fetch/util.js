import {
  PageNotFoundException,
  HttpException,
  DevProxyException,
  UserExpiredException,
} from '../exception';

export async function doGet(url, disableCookie = false) {
  const options = {
    method: 'GET',
  };
  if (disableCookie) {
    options.credentials = 'omit';
  }
  const response = await doFetch(url, options);
  return response;
}

export async function doPost(url, body, disableCookie = false) {
  const headers = new Headers();
  headers.set('Content-Type', 'application/json');
  const options = {
    headers,
    method: 'POST',
    cache: 'no-cache',
    body: typeof body === 'object' ? JSON.stringify(body) : body,
  };
  if (disableCookie) {
    options.credentials = 'omit';
  }
  const response = await doFetch(url, options);
  return response;
}

// export async function doPostText(url, body, disableCookie = true) {
//   const options = {
//     method: 'POST',
//     cache: 'no-cache',
//     body,
//   };
//   if (disableCookie) {
//     options.credentials = 'omit';
//   }
//   const response = await doFetch(url, options);
//   return response;
// }

export async function doPut(url, body = {}, disableCookie = false) {
  const options = {
    method: 'PUT',
    cache: 'no-cache',
    body: typeof body === 'object' ? JSON.stringify(body) : body,
  };
  if (disableCookie) {
    options.credentials = 'omit';
  }
  const response = await doFetch(url, options);
  return response;
}

// export async function doPutText(url, body, disableCookie = true) {
//   const options = {
//     method: 'PUT',
//     cache: 'no-cache',
//     body: body,
//   };
//   console.log(options);
//   if (disableCookie) {
//     options.credentials = 'omit';
//   }
//   const response = await doFetch(url, options);
//   return response;
// }

export async function doDelete(url, body, disableCookie = false) {
  const options = {
    method: 'PUT',
    cache: 'no-cache',
  };
  if (disableCookie) {
    options.credentials = 'omit';
  }
  const response = await doFetch(url, options);
  return response;
}

export async function doFetch(url, options) {
  //   setAuthorizationHeaderForXnat(options);
  if (!options) {
    options = {};
  }
  try {
    options.redirect = 'error';
    const response = await fetch(url, options);
    if (!response.ok) {
      if (response.status === 401) {
        throw new UserExpiredException();
      } else if (response.status === 404) {
        throw new PageNotFoundException();
      } else if (
        response.status === 504 &&
        process.env.NODE_ENV === 'development'
      ) {
        throw new DevProxyException();
      }
      let body = undefined;
      try {
        body = await response.text();
        // eslint-disable-next-line no-empty
      } finally {
      }
      throw new HttpException(response.status, response.statusText, body);
    }
    return response;
  } catch (error) {
    if (
      error instanceof UserExpiredException ||
      error instanceof HttpException ||
      error instanceof PageNotFoundException ||
      error instanceof DevProxyException
    ) {
      throw error;
    } else {
      if (
        process.env.NODE_ENV === 'development' &&
        error.message === 'Failed to fetch'
      ) {
        throw new DevProxyException();
      }
      throw new HttpException(error.status, error.message);
    }
  }
}

function setAuthorizationHeaderForXnat(options) {
  const [token, tokenSecret] = getTokenAndSecret();
  if (token && tokenSecret) {
    const authString = `${token}:${tokenSecret}`;
    if (!options.headers) {
      options.headers = new Headers();
    }
    options.headers.set('Authorization', 'Basic ' + btoa(authString));
  }
}

export function getBasicAuthHeader(username, password) {
  const headers = new Headers();
  const authString = `${username}:${password}`;
  headers.set('Authorization', 'Basic ' + btoa(authString));
  return headers;
}

export function getBasicAuthHeaderWithToken() {
  const [token, tokenSecret] = getTokenAndSecret();
  if (!token || !tokenSecret) {
    return undefined;
  }
  const authString = `${token}:${tokenSecret}`;
  return 'Basic ' + btoa(authString);
}

function getTokenAndSecret() {
  const user = window.store.getState().authentication.user;
  if (!user.token || !user.tokenSecret) {
    return [undefined, undefined];
  } else {
    return [user.token, user.tokenSecret];
  }
}
