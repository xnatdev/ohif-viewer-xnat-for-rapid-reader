import { doGet, doPost } from './util';

export async function fetchSurveyTemplateQuestions(xnatUrl, templateId) {
  const url = new URL(
    `${xnatUrl}/xapi/surveyTemplates/${templateId}/questions`
  );

  const response = await doGet(url.href);
  return await response.json();
}

export async function fetchSurvey(xnatUrl, workListId, surveyType) {
  const url = new URL(
    `${xnatUrl}/xapi/workLists/${workListId}/surveys/${surveyType}`
  );

  const response = await doGet(url.href);
  try {
    return await response.json();
  } catch (err) {
    return undefined;
  }
}

export async function fetchPostSurvey(xnatUrl, workListId, surveyType, form) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/surveys/${surveyType}`;
  const response = await doPost(url, { ...form });
  return await response.json();
}
