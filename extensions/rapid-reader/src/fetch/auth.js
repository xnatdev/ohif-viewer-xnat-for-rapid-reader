import {
  BadCredentialException,
  HttpException,
  BadGatewayException,
} from '../exception';
import { doGet, doPut, doFetch, getBasicAuthHeader } from './util';

const MIN_KEY_LEN = 32;
const MAX_KEY_LEN = 64;

export async function fetchLogin(xnatUrl, username, password) {
  const url = `${xnatUrl}/data/services/auth`;
  const body = `username=${username}&password=${password}`;

  try {
    const response = await doPut(url, body);
    const sessionId = await response.text();
    validateSessionId(sessionId);

    return sessionId;
  } catch (error) {
    if (
      error instanceof HttpException &&
      error.body &&
      error.body.indexOf('Bad credentials') >= 0
    ) {
      throw new BadCredentialException();
    } else {
      throw error;
    }
  }
}

export async function fetchTokenIssue(xnatUrl, username, password) {
  const url = `${xnatUrl}/data/services/tokens/issue`;
  const headers = getBasicAuthHeader(username, password);
  const response = await fetch(url, { headers, credentials: 'omit' });
  if (!response.ok) {
    if (response.status === 401) {
      throw new BadCredentialException();
    } else if (response.status === 502 || response.status === 504) {
      throw new BadGatewayException();
    }
  }
  return await response.json();
}

export async function fetchTokenInvalidate(xnatUrl, token, secret) {
  const url = `${
    xnatUrl ? xnatUrl : '/xnat'
  }/data/services/tokens/invalidate/${token}/${secret}`;
  await doGet(url);
}

function validateSessionId(sessionId) {
  if (sessionId.length < MIN_KEY_LEN) {
    throw new Error('Invalid session id coming');
  }
  if (sessionId.length > MAX_KEY_LEN) {
    throw new Error('Unknown error happened');
  }
}

export async function fetchCSRFToken() {
  const url =
    process.env.NODE_ENV === 'development'
      ? `${window.location.origin}${process.env.XNAT_PROXY}`
      : '/';
  const response = await doFetch(url);
  const html = await response.text();

  const parser = new DOMParser();
  const doc = parser.parseFromString(html, 'text/html');
  const childNodes = doc.childNodes;

  let htmlNode;
  for (let i = 0; i < childNodes.length; i++) {
    if (childNodes[i] instanceof HTMLElement) {
      htmlNode = childNodes[i];
      break;
    }
  }

  const csrfToken = htmlNode.innerHTML.split("csrfToken = '")[1].split("'")[0];
  console.log('csrf', csrfToken);
  return csrfToken;
}
