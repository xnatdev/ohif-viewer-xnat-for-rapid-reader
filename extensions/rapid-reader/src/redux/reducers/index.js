import authentication from './authentication';
import rapidReader from './rapidReader';

const reducers = { authentication, rapidReader };

export default reducers;
