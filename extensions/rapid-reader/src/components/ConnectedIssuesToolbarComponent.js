import { connect } from 'react-redux';
import IssuesToolbarComponent from './IssuesToolbarComponent';
import { withModal } from '@ohif/ui/src/contextProviders/ModalProvider';
import { withSnackbar } from '@ohif/ui/src/contextProviders/SnackbarProvider';
import { fetchCreateComment } from '../fetch/comment';

const mapStateToProps = state => {
  const { rapidReader, authentication } = state;
  const workList = rapidReader.workList;
  const items = rapidReader.workList.items;
  const currentWorkItemIdx = rapidReader.currentWorkItemIdx;
  const workItem = items[currentWorkItemIdx];
  const { xnatUrl, email: fromEmail } = authentication.user;

  return {
    xnatUrl,
    fromEmail,
    workList,
    workItem,
    fetchCreateComment,
  };
};

const ConnectedIssuesToolbarComponent = connect(
  mapStateToProps,
  null
)(IssuesToolbarComponent);

export default withSnackbar(withModal(ConnectedIssuesToolbarComponent));
