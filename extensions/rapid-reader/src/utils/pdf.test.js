import { createPdf } from './pdf';

describe('pdf.js', () => {
  describe('pdf file creation', () => {
    it('should product a valid pdf file', async () => {
      const output = createPdf('Aaron', 11, new Date(), [
        {
          projectLabel: '180-1',
          experimentLabel: 'M00261317_20080105161534',
          reportAssessorId: 'assessorId',
          status: 'Completed',
          age: 3,
          month: 6,
          failReason: 'desc',
        },
      ]);
      expect(output instanceof Blob).toBeTruthy();
    });
  });
});
