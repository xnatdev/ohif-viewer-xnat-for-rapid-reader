import { jsPDF } from 'jspdf';
import 'jspdf-autotable';

const totalPagesExp = '{total_pages_count_string}';

const attestation =
  'By signing this document, I attest that I alone performed the listed bone age interpretations per the study reading protocol.';

export function createPdf(readerName, workListId, finishedDate, workItems) {
  const doc = new jsPDF();

  const rows = workItems.map((workItem, idx) => [
    idx + 1,
    workItem.projectLabel,
    workItem.experimentLabel,
    workItem.reportAssessorId,
    workItem.status,
    workItem.year,
    workItem.month,
    workItem.failReason,
  ]);

  const locale = 'en-US';
  const timeZone = 'America/Chicago';

  const finishedFormatted =
    finishedDate.toLocaleDateString(locale, {
      timeZone,
    }) +
    ' ' +
    finishedDate.toLocaleTimeString(locale, {
      timeZone,
    });
  let y = 22;

  doc.setFontSize(18);
  doc.text('Rapid Reader Summary Report', 58, y);
  doc.setFontSize(11);
  doc.setTextColor(100);

  y += 15;
  doc.setFontSize(11);
  doc.setTextColor(0);
  const splitAtt = doc.splitTextToSize(attestation, 180);
  doc.text(splitAtt, 14, y);
  doc.setFontSize(11);
  doc.setTextColor(100);

  y += 15;
  doc.setTextColor(0);
  doc.text('Work List Id: ', 14, y);
  doc.setTextColor(100);
  doc.text(workListId.toString(), 40, y);

  y += 7;
  doc.setTextColor(0);
  doc.text('# of Work Items: ', 14, y);
  doc.setTextColor(100);
  doc.text(workItems.length.toString(), 45, y);

  y += 7;
  doc.setTextColor(0);
  doc.text('Finished on: ', 14, y);
  doc.setTextColor(100);
  doc.text(finishedFormatted, 38, y);

  y += 8;
  doc.setTextColor(0);
  doc.text('Reader Name: ', 14, y);
  doc.setTextColor(100);
  doc.text(readerName, 42, y);

  //   y += 8;
  //   doc.setTextColor(0);
  //   doc.text('Signature: ', 14, y);

  y += 10;
  doc.autoTable({
    head: [
      [
        'No.',
        'Project',
        'Label',
        'Assessor',
        'Status',
        'Year',
        'Month',
        'Desc',
      ],
    ],
    body: rows,
    startY: y,
    showHead: 'firstPage',
    rowPageBreak: 'auto',
    bodyStyles: { valign: 'top' },
    didDrawPage: function(data) {
      // Footer
      var str = 'Page ' + doc.internal.getNumberOfPages();
      // Total page number plugin only available in jspdf v1.0+
      if (typeof doc.putTotalPages === 'function') {
        str = str + ' of ' + totalPagesExp;
      }
      doc.setFontSize(10);

      // jsPDF 1.4+ uses getWidth, <1.4 uses .width
      var pageSize = doc.internal.pageSize;
      var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
      doc.text(str, (pageSize.getWidth() - 20) / 2, pageHeight - 10);
    },
  });

  // Total page number plugin only available in jspdf v1.0+
  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }

  return doc.output('blob');
}
